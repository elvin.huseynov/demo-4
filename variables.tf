variable "region" {
  default = "us-east-2"
}

variable "access_key" {
  type = string
  sensitive = true
}

variable "secret_key" {
  type = string
  sensitive = true
}

variable "cluster_name" {
  default = "demo4-clst"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "private_subnets" {
  type = list(string)
  default = ["192.168.1.0/24", "192.168.2.0/24", "192.168.3.0/24"]
}

variable "public_subnets" {
  type = list(string)
  default = ["192.168.4.0/24", "192.168.5.0/24", "192.168.6.0/24"]
}

variable "vpc_cidr" {
  default = "192.168.0.0/16"
}

variable "vpc_name" {
  default = "demo4-vpc"
}

variable "worker_cidrs" {
  type = list(string)
  default = ["192.168.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
}

variable "asg_desired_capacity" {
  type = number
  default = 1
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "2121",
    "3333",
  ]
}

variable "map_roles" {
  description = "AWS auth for configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::2121:role/role1"
      username = "role1"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "AWS auth config user"
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::2121:user/user1"
      username = "user1"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::2121:user/user2"
      username = "user2"
      groups   = ["system:masters"]
    },
  ]
}
